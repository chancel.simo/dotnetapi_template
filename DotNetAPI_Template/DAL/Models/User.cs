
using System.ComponentModel.DataAnnotations.Schema;

namespace DotNetAPI_Template.DAL.Models;

[Table("users")]
public class User
{
    public int ID { get; set; }
    public string LastName { get; set; } = string.Empty;

    public string FirstName { get; set; } = string.Empty;
}
