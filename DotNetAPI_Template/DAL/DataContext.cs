using Microsoft.EntityFrameworkCore;
using DotNetAPI_Template.DAL.Models;
using DotNetAPI_Template.Config;

namespace DotNetAPI_Template.DAL;

public class DataContext : DbContext
{
    public DataContext(DbContextOptions<DataContext> options) : base(options) { }

    public DbSet<User> UsersDbSet { get; set; }

    public static ILoggerFactory MyLoggerFactory;
    protected void ConfigureForMysql(DbContextOptionsBuilder optionsBuilder)
    {
        var connectionString = Config.ConfigurationManager.MySqlConnectionString;
        optionsBuilder.UseMySql(connectionString, MySqlServerVersion.AutoDetect(connectionString),
        x =>
        {
            x.CommandTimeout(60);
        });
        optionsBuilder.EnableSensitiveDataLogging(true);
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseLoggerFactory(MyLoggerFactory);
        ConfigureForMysql(optionsBuilder);
    }
}