using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using DotNetAPI_Template.DAL;
using DotNetAPI_Template.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace DotNetAPI_Template.Controllers;

[Route("api/[controller]")]
[ApiController]
public class UserController : ControllerBase
{
    private readonly DataContext _context;
    public UserController(DataContext context)
    {
        _context = context;
    }

    [HttpGet]
    public async Task<ActionResult<List<User>>> GetUser()
    {
        return Ok(await _context.UsersDbSet.ToListAsync());
    }
}